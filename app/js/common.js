$(document).ready(function () {
    $('.menu-icon').click(function () {
        $('nav').slideToggle(600);
    });

});
$(document).ready(function(){
    $('a[href*=#]').bind("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1000);
        e.preventDefault();
    });
    return false;
});
//pop-up
$(document).ready(function () {
    $('.button').click(function () {
        $('.pop-up_bg').toggle();
        $('.pop-up').toggle();
    });
    $('.fas').click(function () {
        $('.pop-up_bg').toggle();
        $('.pop-up').toggle();
    });
    $('.pop-up_bg').click(function(){
        $('.pop-up_bg').toggle();
        $('.pop-up').toggle();
    });

});
